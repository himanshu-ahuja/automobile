package com.javadomain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//Pass purchase code, purchase date, vendor code, vendor name as command line arguments
public class Purchase {

        private short purchaseCode;
        private int purchaseQuantity;
        private short vendorCode;
        private String vendorName;
        private float purchaseAmount;
        private String purchaseDate;
        private String purchaseDescription;

        public void readDetails(String args[]) throws IOException {
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

            /*System.out.println("Enter the Purchase Code: ");
            purchaseCode = Short.parseShort(br.readLine());

            System.out.println("Enter the Purchase Date (DD-MM-YY format): ");
            purchaseDate = br.readLine();

            System.out.println("Enter the Vendor Code: ");
            vendorCode = Short.parseShort(br.readLine());
             */

            //Command Line Argruments
            purchaseCode = Short.parseShort(args[0]);
            vendorCode = Short.parseShort(args[1]);
            purchaseDate = args[2];
            vendorName = args[3];

            System.out.println("Enter Purchase Description: ");
            purchaseDescription = br.readLine();

            System.out.println("Enter Purchased Quantity: ");
            purchaseQuantity = Integer.parseInt(br.readLine());

            System.out.println("Enter the Purchased Amount: ");
            purchaseAmount = Float.parseFloat(br.readLine());

        }//End of Read method

        public void displayDetails() {
            System.out.println("********Purchase Details********");
            System.out.println("PURCHASE CODE: "+purchaseCode);
            System.out.println("PURCHASE DATE: "+purchaseDate);
            System.out.println("VENDOR CODE: "+vendorCode);
            System.out.println("VENDOR NAME: "+vendorName);
            System.out.println("PURCHASE DESCRIPTION: "+purchaseDescription);
            System.out.println("PURCHASE QUANTITY: "+purchaseQuantity);
            System.out.println("TOTAL PURCHASE AMOUNT: "+purchaseAmount);
        } //End of Display Method

        /*Search by Purchase Code*/
         boolean psearch(short pCode){
            if(purchaseCode == pCode){
                return true;
            }
            else return false;
        }

        /*Search by Vendor Code*/
        boolean search(short vCode){
            if(vendorCode == vCode){
                return true;
            }
            else return false;
        }

        /*Search by Purchase Date*/

        boolean search(String pDate){
            if(purchaseDate.equals(pDate)){
                return true;
            }
            else return false;
        }
        public static void main(String args[]) throws IOException
        {
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            boolean choice=true,search=false;
            byte response,response1;

            //Purchase obj[] = new Purchase[2];
            Purchase obj = new Purchase();
            while(choice)
            {
                System.out.println("1.Enter Purchase Details");
                System.out.println("2.Display Purchase Details");
                System.out.println("3.Search Data");
                System.out.println("3.Exit ");
                System.out.println("Enter your choice: ");
                response=Byte.parseByte(br.readLine());
                switch(response)
                {
                    case 1:
//                        for(byte i=0;i<2;i++) {
//                            obj[i]=new Purchase();
//                            obj[i].readDetails(args);
//                        }
                        obj.readDetails(args);
                        break;
                    case 2:
//                        for(byte i=0;i<2;i++) {
//                            obj[i].displayDetails();
//                        }
                        obj.displayDetails();
                        break;

                    case 3:
                        System.out.println("1.BY PURCHASE CODE");
                        System.out.println("2.BY PURCHASE DATE");
                        System.out.println("3.BY VENDOR CODE");
                        System.out.println("Enter your choice: ");
                        response1=Byte.parseByte(br.readLine());

                        switch(response1)
                        {
                            case 1:
                                short pCode;
                                System.out.println("Enter the puchase code: ");
                                pCode = Short.parseShort(br.readLine());
                                //for(byte i=0;i<2;i++) {
                                    search=obj.psearch(pCode);
                                    if(search==true)
                                    {
                                        obj.displayDetails();
                                        break;
                                    }
                                //}
                                if(search==false)
                                {
                                    System.out.println("Purchase record not available");
                                }
                                break;
                            case 2:
                                String pdate;
                                System.out.println("Enter the Date of Purchase: ");
                                pdate = br.readLine();
                                //for(byte i=0;i<2;i++) {
                                    search=obj.search(pdate);
                                    if(search==true)
                                    {
                                        obj.displayDetails();
                                        break;
                                    }
                                //}
                                if(search==false)
                                {
                                    System.out.println("Purchase record not available");
                                }
                                break;
                            case 3:
                                short vCode;
                                System.out.println("Enter the Vendor Code: ");
                                vCode = Short.parseShort(br.readLine());
                                for(byte i=0;i<2;i++)
                                //{
                                    search = obj.search(vCode);
                                    if(search==true)
                                    {
                                        obj.displayDetails();
                                        break;
                                    }
                                //}
                                if(search==false)
                                {
                                    System.out.println("record not available");
                                }
                                break;
                        }// INNER SWITCH CLOSE
                    case 4:
                        choice=false;
                }// END OF SWITCH
            }//End of While
        }//End of Main

}
