package com.javadomain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Customer {

        private String custName;
        private byte custCode;
        private String city;
        private String address;
        private String email;
        private String vehicleModel;

        public void read()throws IOException
        {
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter Customer Name: ");
            custName=br.readLine();

            System.out.println("Enter Customer ID: ");
            custCode=Byte.parseByte(br.readLine());

            System.out.println("Enter City: ");
            city=br.readLine();

            System.out.println("Enter customer address: ");
            address=br.readLine();

            System.out.println("Enter customer emaidID: ");
            email=br.readLine();

            System.out.println("Enter the vehicle model of Customer :");
            vehicleModel=br.readLine();

        }
        public void display()
        {
            System.out.println("Customer Name: "+custName);
            System.out.println("Customer Code: "+custCode);
            System.out.println("Customer City: "+city);
            System.out.println("Customer Address: "+address);
            System.out.println("Customer EmailId: "+email);
            System.out.println("Customer Vehicle model: "+vehicleModel);
        }


        public static void main(String args[]) throws IOException
        {
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            boolean ch=true;
            byte res;

            Customer cust[]=new Customer[2];

            while(ch)
            {
                System.out.println("1. Read Data");
                System.out.println("2. Display Data");
                System.out.println("3. Exit");
                System.out.println("Enter Your Choice: ");
                res=Byte.parseByte(br.readLine());
                switch(res)
                {
                    case 1:
                        for(byte i=0;i<2;i++)
                        {
                            cust[i]=new Customer();
                            cust[i].read();
                        }
                        break;
                    case 2:
                        for(byte i=0;i<2;i++)
                        {
                            cust[i].display();
                        }
                        break;
                    case 3:
                        ch=false;

                }
            }
        }
    }

