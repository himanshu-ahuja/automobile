package com.javadomain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Invoice {

    private String customerName;
    private String invoiceDate;
    private short invoiceCode;
    private String vehicleNumber;
    private String itemName;
    private long itemID;
    private double amount;
    private double amount_disc;
    private float per_unitCost;
    private int quantity;
    private String customerAddress;
    private float discount = 0;
    static short sinvoiceCode;
    static long sitemID;


    static {
            sinvoiceCode = 7800;
            sitemID = 800;
    }
    /*Constructor Overloading*/
    public Invoice(){
        sinvoiceCode++;
        sitemID++;
        invoiceCode = sinvoiceCode;
        itemID = sitemID;

        customerName = "Bruce Wayne";
        invoiceDate = "18-06-2017";
        vehicleNumber = "KA 50L 8970";
        itemName = "Alloy Wheels";
        amount = 27899.6;
        amount_disc = 1200;
        per_unitCost =(float) 6974.9;
        quantity = 4;
        customerAddress = "Christ University";
        discount = 7;
    }

    public Invoice(String name, String invDate, short invCode, String vehNumber){
        customerName = name;
        invoiceDate = invDate;
        invoiceCode = invCode;
        vehicleNumber = vehNumber;
        itemName = "Music System";
        itemID = 802;
        amount = 30000;
        amount_disc = 1500;
        per_unitCost =(float) 15000;
        quantity = 2;
        customerAddress = "Hebbal";
        discount = 10;
    }

    public void readDetails() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter customer name: ");
        customerName = br.readLine();

        //System.out.println("Enter invoice date: ");
        //invoiceDate = br.readLine();
        invoiceDate = "21-06-2017";

        System.out.println("Enter customer vehicle registeration number: ");
        vehicleNumber = br.readLine();

        //System.out.println("Enter item ID: ");
        //itemID = Long.parseLong(br.readLine());
        itemID += 1;
        invoiceCode += 1;

        System.out.println("Enter item name: ");
        itemName = br.readLine();

        System.out.println("Enter per unit cost: ");
        per_unitCost = Float.parseFloat(br.readLine());

        System.out.println("Enter total quantity purchased: ");
        quantity = Integer.parseInt(br.readLine());
        amount = quantity * per_unitCost;

        System.out.println("Enter Customer Address: ");
        customerAddress = br.readLine();

    }//End of readDetails()

    public void displayInvoice() {
        double total;
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(2);
        System.out.println("********Customer Invoice********");
        System.out.println("Customer Name: " + customerName);
        System.out.println("\t\tInvoice Date: " + invoiceDate);
        System.out.println("\t\tInvoice Code: " + invoiceCode);
        System.out.println("Customer vehicle reg. number: " + vehicleNumber);
        System.out.println("\t\tCustomer Address: " + customerAddress);
        System.out.println("Item id: " + itemID);
        System.out.println("Item name: " + itemName);
        System.out.println("Total quantity purchased: " + quantity);
        System.out.println("Per unit cost: " + per_unitCost);
        System.out.println("Discount received: " + amount_disc);
        System.out.println("Tax paid: " + df.format(Calculate(amount)));
        total = Calculate(amount) + amount;
        System.out.println("Total bill amount: " + df.format(total - amount_disc));
    } //End of Invoice Method

    /*Method Overloading*/
    boolean search(String vehicleReg){
            if(vehicleNumber.equals(vehicleReg)){
                return true;
            }
            else return false;
    }

    boolean search(short billCode){
        if(invoiceCode == billCode){
            return true;
        }
        else return false;
    }

    public double Calculate() {
        //Discount Calculate
        if (quantity > 20 && quantity < 100) {
            discount = 15;
        } else if (quantity > 100) {
            discount = 20;
        } else discount = 2;
        amount_disc = (amount * discount) / 100;
        return amount;
    }//End of discountCalculate()

    public double Calculate(double amount){
        //Tax Calculation
        float vat = 14,serviceTax = (float) 5.6, serviceCharge = 5;
        double totalAmount;
        totalAmount = (amount * vat)/100;
        totalAmount += (amount * serviceTax)/100;
        totalAmount += (amount * serviceCharge)/100;
        return totalAmount;
    }
    public static void main(String[] args) throws IOException {
        Invoice inv = new Invoice();
        inv.displayInvoice();
        Invoice inv1 = new Invoice("Thomas Wayne", "19-06-2017", (short) 7802, "KA 09L 8972");
        inv1.displayInvoice();

        Invoice inv_arr[] = new Invoice [3];
        inv_arr[0] = inv;
        inv_arr[1] = inv1;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean choice = true, resultInner = false;
        byte response, inner;

        while (choice) {
            System.out.println("1.Generate Invoice");
            //System.out.println("2.Display Invoice");
            System.out.println("2.Search");
            System.out.println("3.Exit ");
            System.out.println("Enter your choice: ");
            response = Byte.parseByte(br.readLine());
            switch (response) {
                case 1:
                    for(byte i=2;i<3;i++) {
                        inv_arr[i] = new Invoice();
                        inv_arr[i].readDetails();
                    }
                    break;
                /*case 2:
                    for(byte i=2;i<4;i++) {
                        inv3[i].discountCalculate();
                        inv3[i].displayInvoice();
                    }
                    break;
                */
                case 2:
                    System.out.println("1.Search by vehicle registeration number");
                    System.out.println("2.Search by invoice code");
                    System.out.println("Enter your choice: ");
                    inner = Byte.parseByte(br.readLine());

                    switch (inner){
                        case 1:
                            String reg;
                            System.out.println("Enter customer vehicle registeration number: ");
                            reg = br.readLine();
                            for (byte i = 0; i < 3; i++) {
                                resultInner = inv_arr[i].search(reg);
                                if (resultInner == true) {
                                    inv_arr[i].displayInvoice();
                                    break;
                                }
                            }
                            if (resultInner == false) {
                                System.out.println("Details not found!");
                            }
                            break;
                        case 2:
                            short code;
                            System.out.println("Enter customer invoice code: ");
                            code = Short.parseShort(br.readLine());
                            for (byte i = 0; i < 3; i++) {
                                resultInner = inv_arr[i].search(code);
                                if (resultInner == true) {
                                    inv_arr[i].displayInvoice();
                                    break;
                                }
                            }
                            if (resultInner == false) {
                                System.out.println("Details not found!");
                            }
                            break;
                    }//End of inner switch
                case 3:
                    choice = false;

            }//End of Switch
        }//End of While

    }//End of main
}//End of class